# Redux + React + Highchart + Socket.io Example

This project provides a simple way to start show graphs projects with simple build configuration.

Projects built with React include support for ES6 syntax, as well as several unofficial / not-yet-final forms of Javascript syntax such as Class Properties and JSX.

## Available Scripts

In the project directory, you can run:

### `npm install`

Install all the dependencies.

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

##To run server script for socket.io

### `cd server`
### `node app.js`

Runs socket server on [http://localhost:3002](http://localhost:3002)

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!


##How NODE installed on Ubuntu 16.04

### `cd ~`
### `curl -sL https://deb.nodesource.com/setup_6.x -o nodesource_setup.sh`
### `nano nodesource_setup.sh`
### `sudo apt-get install nodejs`
### `sudo apt-get install build-essential`



##How ES installed on elasticsearch on Ubuntu 16.04

### Download latest deb from https://www.elastic.co/downloads/elasticsearch

### https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-5.6.0.deb

### dpkg -i elasticsearch-5.6.0.deb


##How Django installed on elasticsearch on Ubuntu 16.04

### sudo apt-get install redis-server 
### sudo service redis-server status
### cd dfio 
### pip3 install -r requirements.txt
### python3 manage.py migrate 
### python3 manage.py runserver


##TODO 


















