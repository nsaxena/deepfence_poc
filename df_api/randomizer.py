import random
import datetime
from esconn import ESConn

methods = ["GET","POST"]
uris = ["manager/html","admin/html"] 
request_headers = [
    {
        "host": "104.197.45.113:8080",
        "content-type": "text/html",
        "accept": "text/html, */*",
        "authorization": "Basic Og==",
        "user-agent": "Mozilla/3.0 (compatible; Indy Library)"
    },
    {
        "host": "104.197.45.114:8080",
        "content-type": "application/json",
        "accept": "application/json, */*",
        "authorization": "Basic Og==",
        "user-agent": "Chrome/3.0 (compatible; Indy Library)"
    },
    {
        "host": "",
        "content-type": "",
        "accept": "text/html, */*",
        "authorization": "Basic Og==",
        "user-agent": "Internet Explorer"
    }
]
source_ips =  ["222.72.135.116","111.72.135.116","222.72.135.116",]

issue_anomaly_description_etc = [
                            {
                                "issue":"request_anomaly",
                                "summary":"Malicious request is detected as follows :- Anomaly -Rogue website crawler ; ",
                                "event_type":"request_anomaly",
                                "anomaly": "request_anomaly",
                                "input_type":"log",
                                "count":1,
                                "description": "Request Anomaly",

                            }
                    ]
severities = ["low","high","medium","critical"]


host_name_or_host = {
    "dev1":["container1","container2","container3",],
    "dev2":["container4","container5"],
    "dev3":["container6","container7","container8","container9"],
    }


def custom_host_container(json_data):
    rand_host = random.choice([k for k in host_name_or_host])
    container = random.choice(host_name_or_host[rand_host])
    json_data['host']= rand_host
    json_data['host_name']= rand_host
    json_data['container_name'] = container
    
    return json_data
random_time_config = {"before_days":2,
                      "after_days":7}
def random_time_generator(json_data):
    before_days = random_time_config["before_days"]*-86400
    after_days = random_time_config["after_days"]*86400
    delta_seconds = random.randint(before_days,after_days)
    dt = datetime.datetime.now()
    rand_date = dt + datetime.timedelta(0,delta_seconds)
    formatted_date = '{year}-{month}-{date}T{hour}:{minute}:{seconds}.{miliseconds}Z'.format(
        year = rand_date.year,
        month = '%02d'%rand_date.month,
        date = '%02d'%rand_date.day,
        hour = '%02d'%rand_date.hour,
        minute = '%02d'%rand_date.minute,
        seconds = '%02d'%rand_date.second,
        miliseconds = str("%d"%rand_date.microsecond)[:3]
        )
    json_data['@timestamp']  = formatted_date
    return json_data

META_RANDOMIZE = [
                  {"key":"method","from":methods},
                  {"key":"severity","from":severities},
                  {"key":"source_ip","from":source_ips},
                  {"key":"uri","from":uris},
                  {"key":"host_name_or_host","custom":custom_host_container},
                  {"key":"request_headers","from":request_headers},
                  {"key":"all","from":issue_anomaly_description_etc},
                  {"key":"random_time_generate","custom":random_time_generator}
                ]


sample_json = {
  "method": "GET",
  "uri": "/manager/html",
  "request_headers": {
    "host": "104.197.45.113:8080",
    "content-type": "text/html",
    "accept": "text/html, */*",
    "authorization": "Basic Og==",
    "user-agent": "Mozilla/3.0 (compatible; Indy Library)"
  },
  "uri_args": {},
  "@version": "1",
  "@timestamp": "2017-09-04T20:40:28.109Z",
  "type": "alert",
  "count": 1,
  "input_type": "log",
  "host": "dev1",
  "summary": "Malicious request is detected as follows :- Anomaly -Rogue website crawler ; ",
  "event_type": "request_anomaly",
  "severity": "medium",
  "issue": "request_anomaly",
  "host_name": "dev1",
  "container_name": "dev1",
  "resource_type": "uri",
  "anomaly": "request_anomaly",
  "description": "Request Anomaly",
  "process_name": "deepfence-proxy:/manager/html",
  "service": "deepfence-proxy:/manager/html",
  "source_ip": "222.72.135.116",
  "geoip": {
    "ip": "222.72.135.116",
    "country_code2": "CN",
    "country_code3": "CHN",
    "country_name": "China",
    "continent_code": "AS",
    "region_name": "23",
    "city_name": "Shanghai",
    "latitude": 31.045600000000007,
    "longitude": 121.3997,
    "timezone": "Asia/Shanghai",
    "real_region_name": "Shanghai",
    "location": [
      121.3997,
      31.045600000000007,
      121.3997,
      31.045600000000007
    ]
  },
  "ip_checked": "false",
  "bad_ip": "false",
  "ip_reputation": "http://www.senderbase.org/lookup/?search_string=222.72.135.116"
}

def randomize_data():
    sample_json_copy = sample_json.copy()
    for record in META_RANDOMIZE:
        key = record['key']
        if key =="all":
            sample_json_copy.update(random.choice(record['from']))
        elif "custom" in record:
            sample_json_copy = record['custom'](sample_json_copy)
            
        else:
            sample_json_copy[key] = random.choice(record['from'])
            #import pdb;pdb.set_trace()
    return sample_json_copy

if __name__ == "__main__":
    records = []
    for i in range(100):
        records.append(randomize_data())
    print ({"data":records})




