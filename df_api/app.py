from flask import Flask,request, redirect, Response
from flasgger import Swagger
import json
from esconn import ESConn, esconn_methods
from custom_exception import InvalidUsage

app = Flask(__name__)
Swagger(app)


@app.route('/')
def index():
    return redirect("/apidocs/index.html", code=302)


@app.route('/methods', methods=['GET'])
def methods():
    """
    This is a service to extract text from image OCR and ICR

    ---
    tags:
      - EL API
    responses:
      405:
        description: bad request (like missing text data)
      200:
        description: with a valid request and response ... uses standard response codes
    """
    
    return Response(json.dumps(esconn_methods()), mimetype='application/json')



@app.route('/search', methods=['POST'])
def search():
    """
    This is a service to extract text from image OCR and ICR

    ---
    tags:
      - EL API
    parameters:
      - name: input_json
        in: body
        type: string
        required: true
        description: Input type
    responses:
      405:
        description: bad request (like missing text data)
      200:
        description: with a valid request and response ... uses standard response codes
    """
    request_data = request.data.decode('utf-8')
    request_json = json.loads(request_data)
    method_name = request_json.get("method",None)
    if not method_name :
        raise InvalidUsage('method param is missing', status_code=410)
    
    if "params" not in request_json :
        raise InvalidUsage('method param is missing', status_code=410)
    params = request_json["params"]
    method_params = [params[e] for e in esconn_methods()[method_name]['params']]
    method =getattr(ESConn, method_name)
    resp =method(*method_params)  

    return Response(json.dumps(resp), mimetype='application/json')

if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True, port=999)
