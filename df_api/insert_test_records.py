from esconn import ESConn
from randomizer import randomize_data

for i in range(1000):
    doc = randomize_data()
    ESConn.create_doc(index_name="deepfence", doc_type="alerts", doc=doc)
    print ("Inserted Doc %s %d "% (doc, i+1))
