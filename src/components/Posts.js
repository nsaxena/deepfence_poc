import React from 'react'
import { Table } from 'react-bootstrap';
import PropTypes from 'prop-types'

const Posts = ({posts, hostFilter}) => (
    <Table responsive>
        <thead>
            <tr>
                <th>Host</th>
                <th>Process Name</th>
                <th>Container Name</th>
                <th>Description</th>
                <th>Type</th>
                <th>Severity</th>
            </tr>
        </thead>
        <tbody>
            {posts.filter(post => { return (!hostFilter || (post.process_name.toLowerCase().search(hostFilter) !== -1) || (post.container_name.toLowerCase().search(hostFilter) !== -1) || (post.description.toLowerCase().search(hostFilter) !== -1) || (post.type.toLowerCase().search(hostFilter) !== -1) || (post.severity.toLowerCase().search(hostFilter) !== -1))}).map((post, i) =>
                <tr key={i}>
                    <td>{post.host_name}</td>
                    <td>{post.process_name}</td>
                    <td>{post.container_name}</td>
                    <td>{post.description}</td>
                    <td>{post.type}</td>
                    <td>{post.severity}</td>
                </tr>
            )}
        </tbody>
    </Table>
)

Posts.propTypes = {
  posts: PropTypes.array.isRequired,
  hostFilter: PropTypes.string.isRequired
}

export default Posts
