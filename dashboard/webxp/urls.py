"""webxp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import url
from django.contrib import admin
from django.views.generic import TemplateView
from dashboard import views as dashboard_views
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$',dashboard_views.home), 
    url(r'^accounts/login/',dashboard_views.xp_login), 
    url(r'^accounts/logout/',dashboard_views.xp_logout), 
    
    url(r'^fields/(?P<index_name>[-\w.]+)/(?P<mapping_name>[-\w]+)',dashboard_views.get_fields), 
    url(r'^aggr_graph/(?P<index_name>[-\w.]+)/(?P<field>[-\w@]+)/(?P<chart_type>[-\w]+)',dashboard_views.aggr_chart), 

    url(r'^save_graph/',dashboard_views.save_graph, name ="save_graph"), 
    
    url(r'^execute_query/(?P<index_name>[-\w.]+)/',dashboard_views.execute_query, name= "execute_query"), 
    
    
    
    
    #
        #TemplateView.as_view(template_name="index.html")),

]
