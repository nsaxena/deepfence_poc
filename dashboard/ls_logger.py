##python setup.py sdist
import os
import logging
from datetime import datetime
from logstash import handler_tcp   # pip install python-logstash-0.4.6
import json
from logging import LogRecord
'''
Author: Shashank 29-Aug-2016
    This module contains log settings for the service. Configuration are implemented through env. variables
    and are expected to be set by the build process in the docker container.
    1. All log connection inforCmations are conceiled in a setting_<db|graph|log> object
    2. Methods to get logger and the setting are in here.
    Note: When the enviornment variables change it requires a restart of the service.
        ( We are currently redeploying the docker with new env variables set.)
        This is done to avoid overhead of reading everytime outside of the process.
        This can be reviewed and modified if needed or partially made dynamic especially for logging levels.
'''

'''
input {

        tcp {
                port => 9700
                type => json
        }

}

filter
{
    mutate
    {
        replace => [ "message", "%{message}" ]
        gsub => [ 'message','\n','']
    }
    if [message] =~ /^{.*}$/
    {
        json { source => message }
    }

}
output {
  elasticsearch {
    'hosts' => ['localhost:9200']
    'index' => 'jgd'
    'codec' => json
  }
}
'''

class CustomTCPLogstashHandler(handler_tcp.TCPLogstashHandler):
    """
    This method is overriden to set the byte values.
    """
    def makePickle(self, record):
        return record.msg.encode()

class LsLogger:
    ls_logger  = logging.getLogger('jjj',)
    
    settings_log={}

    def __init__(self):
        self.pop_setting_log()
        self.ls_logger.setLevel(self.settings_log['loglevel'])
        handler_obj = CustomTCPLogstashHandler(self.settings_log['logstashhost'], int(self.settings_log['logstashport']), version=1, message_type='json')
        #handler_obj.formatter = LogstashFormatterV1()
        self.ls_logger.addHandler(handler_obj)  # os.environ['LOGSTASH_PORT'], version=1))

    LEVELS = {'debug': logging.DEBUG,
              'info': logging.INFO,
              'warning': logging.WARNING,
              'error': logging.ERROR,
              'critical': logging.CRITICAL}

    def pop_setting_log(self):
        if self.settings_log.__len__() == 0:
            try:
                self.settings_log = {
                    'logstashhost': '127.0.01', #os.environ['LOGSTASH_HOST'],
                    'logstashport': 9700, #os.environ['LOGSTASH_PORT'],
                    #'loglevel': LsLogger.LEVELS.get('info', logging.NOTSET)
                    'loglevel': self.LEVELS.get(os.environ['LOGSTASH_LOG_LEVEL'], logging.NOTSET)  # export LOG_LEVEL=info
                }
            except KeyError:
                print(
                    "Please set the environment variable LOGSTASH_HOST and LOG_LEVEL... setting to default ")  # tbd log error
                self.settings_log = {"logstashhost": "ec2-54-164-199-152.compute-1.amazonaws.com", "logstashport": "19000" , "loglevel": logging.INFO }
        return self.settings_log   # for unit testing purpose
        # else:
        #     return self.settings_log
        # return self.settings_log

    def log(self, loglevel, reqid, sid, tenid, dt, cnm , mnm, text, jsondata, ):
        if loglevel == "info":
            logmethod_nm = self.ls_logger.info
        elif loglevel == "debug":
            logmethod_nm = self.ls_logger.debug
        elif loglevel == "warning":
            logmethod_nm = self.ls_logger.warning
        elif loglevel == "error":
            logmethod_nm = self.ls_logger.error
        elif loglevel == "critical":
            logmethod_nm = self.ls_logger.critical
        else:
            raise Exception('ERROR: from' + cnm + "  " + mnm +" loglevel has to be info|debug|warning|error|critical")
#         logmethod_nm(text, extra={ 'data': {'Reqid [{}] Ssid [{}] Tenid [{}] Dt [{}] CNm [{}] MNm [{}] data [{}]'.format(reqid, sid, tenid,   \
#                       str(datetime.now()), cnm, mnm, jsondata)}})
        
        dd = {"Reqid":reqid,"Ssid":sid,"Tenid":tenid,"Dt":str(datetime.now()),
                                 "CNm":cnm , "MNm":mnm,
                                 "text":text
                                 }
        dd.update(jsondata)
        print ("== Logging ==")
        print (dd)
        logmethod_nm(json.dumps(dd))
            
            
#             '{ "Reqid":"{0}", "Ssid":"{1}", "Tenid":"{2}", "Dt" [{}] CNm [{}] MNm [{}] data [{}]'.format(reqid, sid, tenid,   \
#                       str(datetime.now()), cnm, mnm, jsondata)}})

        
    def test(self):
        self.log('info', "9999", "99999", "999999",  \
                  str(datetime.now()), "LsLogger" ,'Test Logging-info', "999-Data")

if __name__ == '__main__':
    l = LsLogger()
    # l.test()
    l.log('info', "9999","99999" ,"999999" ,  \
           str(datetime.now()),"__main__", 'Test Logging-error',"999-Data",{"name":"Nishant", "address":"Ameeerpeth"})
    #l.test()