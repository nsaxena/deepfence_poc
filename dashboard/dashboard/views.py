from django.shortcuts import render
from django.template.response import TemplateResponse
from django.contrib.auth.decorators import login_required
from django.contrib import messages 
from django.contrib.auth import authenticate, login, logout
from django.http.response import HttpResponseRedirect, JsonResponse
from webxp.settings import EL_CLIENT, EL_HOST
from django.views.decorators.csrf import csrf_exempt
from dashboard.models import SavedGraphs

#'client','index','chart_type','field','type_graph')
@login_required
@csrf_exempt
def execute_query(request,index_name):
    query = eval(request.POST.get('query')) 
    res = EL_CLIENT.search(index=index_name,body=query)
    null =None
    false = False 
    true = True
    #@messages = mm = [e['_source']['message'] for e in res['hits']['hits'] ] 
    #import pdb;pdb.set_trace()
    return JsonResponse(res)
# Create your views here.
@csrf_exempt
@login_required
def save_graph(request):
    
    ## TODO : Fix unique together is not working. 
    existing = SavedGraphs.objects.filter(user=request.user,
                client = request.POST.get("client"),
                index = request.POST.get("index"),
                chart_type = request.POST.get("field"),
                type_graph = request.POST.get("type_graph"),
                )
    
    if existing:
        return JsonResponse({"error":"Same graph saved, name: "+existing.saved_name()})
    else:
        SavedGraphs(user=request.user,
                client = request.POST.get("client"),
                index = request.POST.get("index"),
                chart_type = request.POST.get("field"),
                type_graph = request.POST.get("type_graph"),
                saved_name = request.POST.get("saved_name")
                ).save()
        

        return JsonResponse({"success":"Saved"})

@login_required
def home(request):
    indices = [e for e in EL_CLIENT.indices.get_settings().keys()  if "kibana" not in e and "filebeat" not in e and (not e.startswith(".monitoring"))]
    indices = dict([(index_name,list(EL_CLIENT.indices.get(index_name)[index_name]['mappings'].keys())) for index_name in indices])    
    #indices = [e for e in indices if "2017.02.16" in e]
    return TemplateResponse(request,"index.html",{"indices":indices,"es_server":EL_HOST})

def get_fields(request,index_name,mapping_name ):
    fields = EL_CLIENT.indices.get(index_name)[index_name]['mappings'][mapping_name]['properties'].keys()
    return JsonResponse({"index_name":index_name,"mapping_name":mapping_name,  "fields":list(fields)})


def aggr_chart(request,index_name,field,chart_type):
    true=True
    aggr_name = 'aggr_alias_name'
    query = {
        "size": 0,
        "query": {
            "query_string": {
                "query": "*",
                "analyze_wildcard": true
            }
        },
        "aggs": {
            aggr_name: {
                "terms": {
                    "field": field,
                    "size": 10,
                    "order": {
                    "_count": "desc"
                    }
                }
            }
        }
    }
    
    res = EL_CLIENT.search(index=index_name,body=query)
    buckets= res['aggregations'][aggr_name]['buckets']
    return JsonResponse({
        "chart_type":chart_type,
        "div_id":"container",
        "chart_title":"Aggregation on %s" %field,
        "subtitle":'Source :  %s' %EL_HOST,
        "yaxis_title":'Count : %s'%field,
        "tooltip_point_format":' <b>{point.y:,.0f}</b><br/>',
        "plot_options_marker":{
                    "enabled": 0,
                    "symbol": 'circle',
                    "radius": 2,
                    "states": {
                        "hover": {
                            "enabled": 1
                        }
                    }
                },
        "categories": [str(e['key']) for e in buckets],
        "series":
        [{
            "name":"Distribution",
            "data":[e['doc_count'] for e in buckets]
            
            }  ]
        })


def xp_login(request):
    if request.method=="POST":
        user = authenticate(username=request.POST.get("username"),password=request.POST.get("password"),)
        if not user:
            messages.error(request, "Username and password, does not match")
        else:
            login(request,user)
            next_page = request.POST.get("next","/")
            return HttpResponseRedirect(next_page)
    return TemplateResponse(request,"login.html",{})


def xp_logout(request):
    logout(request)
    return HttpResponseRedirect("/")
    
