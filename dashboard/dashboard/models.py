from django.db import models
from django.contrib.auth.models import User
CHOICES_GRAPH_TYPES = (
            ("aggr_graph","Aggr Graph"),
            )
# Create your models here.
class SavedGraphs(models.Model):
    user = models.ForeignKey(User)
    client = models.CharField(max_length=128)
    index = models.CharField(max_length=128)
    chart_type = models.CharField(max_length=128)
    field = models.CharField(max_length=128)
    type_graph = models.CharField(max_length=128,choices = CHOICES_GRAPH_TYPES, null=True)
    saved_name = models.CharField(max_length=128)


    class Meta:
        unique_together = ('user', 'client','index','chart_type','field','type_graph')

