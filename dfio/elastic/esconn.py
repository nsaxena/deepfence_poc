import os

'''
Following must be set, to create Elastic connection.
    ELASTICSEARCH_HOST
    ELASTICSEARCH_PORT
    ELASTICSEARCH_USER
    ELASTICSEARCH_PASSWORD
'''

from elasticsearch import Elasticsearch

EL_HOST = "http://%s:%s" % (os.environ['ELASTICSEARCH_HOST'],
                            os.environ['ELASTICSEARCH_PORT'])

http_auth = None
if 'ELASTICSEARCH_USER' in os.environ:
    http_auth = (os.environ['ELASTICSEARCH_USER'],
                               os.environ['ELASTICSEARCH_PASSWORD'])
if http_auth:
    EL_CLIENT = Elasticsearch([EL_HOST],http_auth=http_auth)
else:
    EL_CLIENT = Elasticsearch([EL_HOST])   

class ESConn:

    @staticmethod
    def get_server_info():
        """
        Check if the ES is connected
        """
        return EL_CLIENT.info()

    @staticmethod
    def create_doc(index_name, doc_type, doc):
        """
        Create a new document.
        """
        res = EL_CLIENT.index(index=index_name, doc_type=doc_type, body=doc)
        return res

    @staticmethod
    def get_doc_by_id(index_name, doc_type, rec_id):
        res = EL_CLIENT.get(index=index_name, doc_type=doc_type, id=rec_id)
        return res

    @staticmethod
    def del_doc_by_id(index_name, doc_type, rec_id):
        res = EL_CLIENT.delete(index=index_name, doc_type=doc_type, id=rec_id)
        return res

    @staticmethod
    def overwrite_doc_having_id(index_name, doc_type, doc, doc_id):
        """
        This method will overwrite the existing record if exist
        say : existing_record = {"name":"John", "city":"SF","_id":1}
        doc is {"name":"John Lee","county":"US"} and doc_id is 1
        than after the operation record will be
         {"name":"John Lee","county":"US","_id":1}
        If record not exist it will be the doc with doc_id as _id.
        """
        res = EL_CLIENT.index(index=index_name, doc_type=doc_type,
                              id=doc_id, body=doc)
        return res

    @staticmethod
    def update_doc_having_id(index_name, doc_type, doc, doc_id):
        """
        This method will update the existing record if exist
        say : existing_record = {"name":"John", "city":"SF","_id":1}
        doc is {"name":"John Lee","county":"US"} and doc_id is 1
        than after the operation record will be
         {"name":"John Lee","county":"US","city":"SF","_id":1}
        If record not exist it will be the doc with doc_id as _id.
        """
        result = ESConn.search_by_and_clause(index_name, doc_type,
                                             {"_id": doc_id})
        if result['total']:
            assert result['total'] == 1
            existing_data = result['hits'][0]['_source']
            existing_data.update(doc)
            res = EL_CLIENT.index(index=index_name, doc_type=doc_type,
                                  id=doc_id, body=existing_data)

        else:
            res = EL_CLIENT.index(index=index_name, doc_type=doc_type,
                                  id=doc_id, body=doc)
        return res

    @staticmethod
    def search_by_and_clause(index_name, doc_type, key_value_dictionary,
                             start_index=0, size=10):
        """
        E.g. if {k1:v1,k2:v2}  is key_value_dictionary
        k1=v1 and k2=v2 ... will be result
        """
        # TODO : Try without constant_score
        # https://www.elastic.co/guide/en/elasticsearch/reference/current/query-filter-context.html

        query = {
          "from": start_index,
          "query": {
            "bool": {
              "filter": [
                {
                  "term": {
                    key: value
                  }
                } for key, value in
                        key_value_dictionary.items()
              ]
            }
          },
          "size": size
        }
        res = EL_CLIENT.search(index=index_name, body=query, doc_type=doc_type)
        return res['hits']

    @staticmethod
    def search(index_name, query, doc_type=None, start_index=0, size=10):
        q = {"from": start_index, "size":size}
        query.update(q)
        res = EL_CLIENT.search(index=index_name, body=query, doc_type=doc_type)
        return res

if __name__ == '__main__':
    pass
