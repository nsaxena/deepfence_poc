from esconn import ESConn


def test_pagination_total(query={},start_index=0, size=10):
    search_all = ESConn.search("deepfence", query, "alerts",start_index=start_index, size=size)
    return {
            "total":search_all['hits']['total'],
            "records_fetched":len(search_all['hits']['hits'])
            }
    
## Assuming more than 100 records.
assert  test_pagination_total(start_index=0, size=100) == {'total': 1000, 'records_fetched': 100}


def test_pagination_last_record(query={},start_index=0, size=10):
    search_all = ESConn.search("deepfence", query, "alerts",start_index=start_index, size=size)
    return {
            "total":search_all['hits']['total'],
            "records_fetched":len(search_all['hits']['hits'])
            }


